document.addEventListener("DOMContentLoaded", (event) => {

	var
		__hostname 	= window.location.hostname,
		__links		= document.getElementsByTagName('a');

	__hostname 		= __hostname.replace("www.","").toLowerCase();

	this.check = (obj) => {
		var href = obj.href.toLowerCase();
		return ((href.indexOf("http://")!=-1 || href.indexOf("https://")!=-1) && href.indexOf(__hostname)==-1) ? true : false;
	}

	this.set = (obj) => {
		obj.target 		= "_blank";
		obj.className 	= obj.className + " external";
		obj.href 		= obj.href;
	}

	for (var i=0;i<__links.length;i++){
		if(this.check(__links[i])) this.set(__links[i]);
	};

});
