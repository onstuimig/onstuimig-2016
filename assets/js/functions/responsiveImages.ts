document.addEventListener("DOMContentLoaded", function(event) {
	var responsiveImages = document.querySelectorAll('.js.responsive-image');

	Array.prototype.forEach.call(responsiveImages, function(element, i){
		new LazyResponsiveImage(element, {
			type: 				'image',
			viewportOffset: 	250,
		});
	});

	var coverImages = document.querySelectorAll('.js.cover-image');

	Array.prototype.forEach.call(coverImages, function(element, i){
		new LazyResponsiveImage(element, {
			type: 				'background',
			viewportOffset: 	250,
			imageSizes: {
	 			481 	:	'mobile',
	 			768 	:	'small',
	 			1024	:	'tablet',
				1200	: 	'desktop-small',
				1650	:	'desktop-large',
	 		}
		});
	});
});
