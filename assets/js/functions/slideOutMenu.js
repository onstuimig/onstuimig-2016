document.addEventListener("DOMContentLoaded", function (event) {
    if (document.getElementById('panel') && document.getElementById('menu') && document.getElementById('menu-trigger')) {
        var slideout = new Slideout({
            'panel': document.getElementById('panel'),
            'menu': document.getElementById('menu'),
            'padding': 256,
            'tolerance': 70,
            'touch': false
        });
        slideout.on('beforeopen', function (event) {
        });
        slideout.on('beforeclose', function () {
        });
        document.getElementById('menu-trigger').addEventListener('click', function (e) {
            e.preventDefault();
            slideout.toggle();
        });
        jQuery('.slideout-panel-close-trigger').on('click tap touchmove', function (e) {
            e.preventDefault();
            slideout.close();
        });
    }
    else {
        console.warn('No correct slideout.js setup defined.');
    }
});
