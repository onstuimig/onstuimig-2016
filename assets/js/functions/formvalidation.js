jQuery(function () {
    if (jQuery('form[validate-form]').length > 0) {
        var myForm = new FormValidate(jQuery('form[validate-form]'));
        myForm.addValidator({
            validatorName: 'hex',
            onEvent: 'keyup',
            logic: function (dataFromTemplate, fieldValue) {
                var pattern = /^#?([a-f0-9]{6}|[a-f0-9]{3})$/;
                return (pattern.test(fieldValue));
            }
        });
    }
});
