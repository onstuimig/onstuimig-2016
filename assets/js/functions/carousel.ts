document.addEventListener("DOMContentLoaded", (event) => {

	var utils = window.fizzyUIUtils;

	if(typeof document.querySelector('.js-projects-switcher') != 'undefined') {

		var flkty = new Flickity(document.querySelector('.js-projects-switcher'), {
			cellAlign: 'center',
			wrapAround: true,
			draggable: (isMobile.any) ? true : false,
			autoPlay: 3000,
			pageDots: false,
			initialIndex: 2,
			freeScroll: false,
			percentPosition: false,
			prevNextButtons: false,
			pauseAutoPlayOnHover: false,
			//selectedAttraction: 0.083,
			//friction: 0.26,
			cellSelector: '.project'
		});

		var getScroll;
		var pixelsScrolled;
		var blokje = document.querySelector('.project.is-selected').offsetHeight;
		var getCarouselHeight = document.querySelector('.page-section--carousel.js').offsetHeight;
		var imagePixelsToScroll;
		var imagePixels;

		function repeatOften() {

			pixelsScrolled = ((getScroll / getCarouselHeight) * 100);
			imagePixels = document.querySelector('.projects-switcher .project.is-selected .scroller').offsetHeight - blokje;

			if(pixelsScrolled < 0) { pixelsScrolled = 0 }
			if(pixelsScrolled >= 100) { pixelsScrolled = 100 }

			imagePixelsToScroll = ((imagePixels / 100) * pixelsScrolled);

			var elem = document.querySelector('.projects-switcher .project.is-selected .scroller');

			var transform = `translateY(-${imagePixelsToScroll}px)`;

			elem.style.WebkitTransform = transform;
			elem.style.msTransform = transform;
			elem.style.transform = transform;
		};

		window.addEventListener("scroll", function(){
		    getScroll = document.documentElement.scrollTop || document.body.scrollTop
		    requestAnimationFrame(repeatOften);
		});

		flkty.element.addEventListener('click', (event) => {
			var clickedCell = flkty.getParentCell( event.target );

			flkty.pausePlayer();

			setTimeout(() => {
				flkty.unpausePlayer();
			}, 3000);

			var cellIndex = clickedCell && utils.indexOf( flkty.cells, clickedCell );
			if ( typeof cellIndex == 'number' ) {
				var len = flkty.cells.length;
				cellIndex = getClosestIndex( cellIndex, flkty.selectedIndex, len );
				cellIndex = getClosestIndex( cellIndex, flkty.selectedIndex, -len );
				flkty.select(cellIndex);
			}
		});

		function getClosestIndex( index, selectedIndex, delta ) {
			var deltaIndex = index + delta;
			return Math.abs( deltaIndex - selectedIndex ) < Math.abs( index - selectedIndex ) ? deltaIndex : index;
		}

		flkty.on( 'cellSelect', () => {

			flkty.element.classList.remove('no-animate');

			for (var i in flkty.cells) {
				flkty.cells[i].element.classList.remove('prev-1');
				flkty.cells[i].element.classList.remove('next-1');
				flkty.cells[i].element.classList.remove('prev-2');
				flkty.cells[i].element.classList.remove('next-2');
				flkty.cells[i].element.classList.remove('active');
			}

			var scroller = flkty.cells[flkty.selectedIndex].element.querySelector('.scroller');
			var transform = `translateY(-${imagePixelsToScroll}px)`;

			scroller.style.WebkitTransform = transform;
			scroller.style.msTransform = transform;
			scroller.style.transform = transform;

			createSiblingClasses();

			flkty.cells[flkty.selectedIndex].element.classList.add('active');
		});

		function createSiblingClasses() {

			if(flkty.selectedIndex == (flkty.cells.length -1)) {

				// laatste
				var prev1 = flkty.cells[flkty.selectedIndex -1].element;
				prev1.classList.add('prev-1');

				var next1 = flkty.cells[0].element;
				next1.classList.add('next-1');

				if(flkty.cells.length >= 5) {
					var prev2 = flkty.cells[flkty.selectedIndex -2].element;
					prev2.classList.add('prev-2');

					var next2 = flkty.cells[1].element;
					next2.classList.add('next-2');
				}

			}else if (flkty.selectedIndex === 0) {

				// first
				var prev1 = flkty.cells[flkty.cells.length -1].element;
				prev1.classList.add('prev-1');

				var next1 = flkty.cells[flkty.selectedIndex +1].element;
				next1.classList.add('next-1');

				if(flkty.cells.length >= 5) {
					var prev2 = flkty.cells[flkty.cells.length -2].element;
					prev2.classList.add('prev-2');

					var next2 = flkty.cells[flkty.selectedIndex +2].element;
					next2.classList.add('next-2');
				}

			}else {

				// normal
				var prev = flkty.cells[flkty.selectedIndex -1].element;
				prev.classList.add('prev-1');

				var next = flkty.cells[flkty.selectedIndex +1].element;
				next.classList.add('next-1');

				if(flkty.cells.length >= 5) {

					if(flkty.selectedIndex === 1) {

						var prev2 = flkty.cells[flkty.cells.length -1].element;
						prev2.classList.add('prev-2');

						var next2 = flkty.cells[flkty.selectedIndex +2].element;
						next2.classList.add('next-2');

					}else if(flkty.selectedIndex === 2) {

						var prev2 = flkty.cells[0].element;
						prev2.classList.add('prev-2');

						var next2 = flkty.cells[flkty.selectedIndex +2].element;
						next2.classList.add('next-2');

					}else if (flkty.selectedIndex == flkty.cells.length - 2) {

						var prev2 = flkty.cells[flkty.selectedIndex -2].element;
						prev2.classList.add('prev-2');

						var next2 = flkty.cells[0].element;
						next2.classList.add('next-2');

					}else {

						var prev2 = flkty.cells[flkty.selectedIndex -2].element;
						prev2.classList.add('prev-2');

						var next2 = flkty.cells[flkty.selectedIndex +2].element;
						next2.classList.add('next-2');
					}
				}
			}
		}

		createSiblingClasses();

	}
});
