jQuery(function () {
    if (typeof blueprint != 'object')
        return;
    var template = twig({
        data: document.body.innerHTML
    });
    document.body.innerHTML = template.render(blueprint);
    document.body.className = document.body.className.replace("twig-is-parsing", "");
});
