var _this = this;
document.addEventListener("DOMContentLoaded", function (event) {
    var __hostname = window.location.hostname, __links = document.getElementsByTagName('a');
    __hostname = __hostname.replace("www.", "").toLowerCase();
    _this.check = function (obj) {
        var href = obj.href.toLowerCase();
        return ((href.indexOf("http://") != -1 || href.indexOf("https://") != -1) && href.indexOf(__hostname) == -1) ? true : false;
    };
    _this.set = function (obj) {
        obj.target = "_blank";
        obj.className = obj.className + " external";
        obj.href = obj.href;
    };
    for (var i = 0; i < __links.length; i++) {
        if (_this.check(__links[i]))
            _this.set(__links[i]);
    }
    ;
});
