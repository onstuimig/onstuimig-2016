jQuery( () => {
	if(jQuery('form[validate-form]').length > 0) {
		var myForm = new FormValidate( jQuery('form[validate-form]'));

		// Nieuwe validatieregel toevoegen
		myForm.addValidator({

			validatorName: 	'hex', 			// To be used in the HTML (validate-props="$validatorName")
			onEvent: 		'keyup', 		// keyup|change
			logic: 	function(dataFromTemplate, fieldValue) {
				var pattern = /^#?([a-f0-9]{6}|[a-f0-9]{3})$/;
				return (pattern.test(fieldValue));
			}
		});
	}
});