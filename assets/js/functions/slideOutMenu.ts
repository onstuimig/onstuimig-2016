document.addEventListener("DOMContentLoaded", (event) => {

	if(document.getElementById('panel') && document.getElementById('menu') && document.getElementById('menu-trigger') ) {

		var slideout = new Slideout({
		  'panel': document.getElementById('panel'),
		  'menu': document.getElementById('menu'),
		  'padding': 256,
		  'tolerance': 70,
		  'touch' : false
		});

		slideout.on('beforeopen', (event) => {
			//jQuery('.js-fixed-header').addClass('has-slided');
		});

		slideout.on('beforeclose', () => {
			//jQuery('.js-fixed-header').removeClass('has-slided');
		});

		document.getElementById('menu-trigger').addEventListener('click', (e) => {
			e.preventDefault();
			slideout.toggle();
		});

		// Attach a 'close' handler to the HTML selector
		jQuery('.slideout-panel-close-trigger').on('click tap touchmove', (e) => {
			e.preventDefault();
			slideout.close();
		});

	}else {
		console.warn('No correct slideout.js setup defined.');
	}

});
