let isMobileDevice: boolean;

document.addEventListener("DOMContentLoaded", (event) => {

	if('CSS' in window && 'supports' in window.CSS) {
	    var support = window.CSS.supports('mix-blend-mode','soft-light');
	        support = support?'mix-blend-mode':'no-mix-blend-mode';
	        document.querySelector('html').classList.add(support);
	}

	if(isMobile.any){
		document.querySelector('html').classList.add('is-mobile-device');
        jQuery('html').addClass('is-mobile-device');
        isMobileDevice = true;
    }else {
        document.querySelector('html').classList.add('is-pointer-device');
		isMobileDevice = false;
    }
});
