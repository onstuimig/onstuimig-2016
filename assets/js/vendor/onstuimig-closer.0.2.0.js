/*!
* Closer.js
* @version 0.2.0
* @requires jQuery.js
* @license Copyright 2014 Onstuimig
*/

jQuery('body').on('click', '[data-closer]' ,function(e){
	if (jQuery(e.target).hasClass('js-no-closer') || jQuery(e.currentTarget).hasClass('js-no-closer') || jQuery(e.target).attr('data-no-closer') == "" || jQuery(e.currentTarget).attr('data-no-closer') == "") {
		return;
	}

	var $this = jQuery(this);
	//if (!$this.data('closer-attached') || typeof $this.data('closer-attached') == 'undefined') {
		var data = $this.data('closer');

		if (typeof options == 'object') {
			if (data == "" || data == false) {
				data = options;
			} else {
				data = jQuery.merge(options,data);
			}
		}

		if (data != "") {
			var fireAfterClose = "";
			if (typeof data.fireAfterClose != "undefined" && data.fireAfterClose != "") {
				fireAfterClose = data.fireAfterClose;
			}

			var $closeSelector = $this.parent();
			if (typeof data.closeSelector != "undefined") {
				if (data.closeSelector.indexOf("parent.parent.parent") == 0) {
					$closeSelector = $this.parent().parent().parent();
					var tmpchild = data.closeSelector.replace("parent.parent.parent ","");
					if (tmpchild != "parent.parent.parent") {
						$closeSelector = $closeSelector.find(tmpchild);
					}
				}
				else if (data.closeSelector.indexOf("parent.parent") == 0) {
					$closeSelector = $this.parent().parent();
					var tmpchild = data.closeSelector.replace("parent.parent ","");
					if (tmpchild != "parent.parent") {
						$closeSelector = $closeSelector.find(tmpchild);
					}
				}
				else if (data.closeSelector.indexOf("parent.") == 0) {
					var tmp = data.closeSelector.replace("parent.","");
					tmp = tmp.split(" ");
					if (tmp.length > 0) {
						$closeSelector = $this.parents('.'+tmp[0]);
					}

					if (tmp.length > 1) {
						var tmpchild = tmp[1];
						if (tmpchild != "parent") {
							$closeSelector = $closeSelector.find(tmpchild);
						}
					}
				}
				else if (data.closeSelector.indexOf("parent") == 0) {
					$closeSelector = $this.parent();
					var tmpchild = data.closeSelector.replace("parent ","");
					if (tmpchild != "parent") {
						$closeSelector = $closeSelector.find(tmpchild);
					}
				}

				if (data.closeSelector === "this") {
					$closeSelector = $this;
					var tmpchild = data.closeSelector.replace("this ","");
					if (tmpchild != "this") {
						$closeSelector = $closeSelector.find(tmpchild);
					}
				}
				if (data.closeSelector === "") {
					$closeSelector = $this;
				}

				if (data.closeSelector !== "this" && data.closeSelector !== "" && data.closeSelector.indexOf("parent") == -1) {

					$closeSelector = jQuery(data.closeSelector);
				}
			} else {
				$closeSelector = $this;
			}

			var $closeType = 'hide';
			var $closeTypeData = 'closed';
			if (typeof data.closeType != "undefined") {
				if (data.closeType === "toggleclass") {
					$closeType = data.closeType;
				}
				if (typeof data.closeTypeData != "undefined") {
					$closeTypeData = data.closeTypeData;
				}
			}

			var close = function(){
				if ($closeType === 'hide') {
					$closeSelector.hide();
				}
				if ($closeType === 'toggleclass') {
					$closeSelector.toggleClass($closeTypeData);
				}

				if (typeof data.cookieName != "undefined") {
					createCookie(data.cookieName+'_'+$closeType,data.cookieValue, $cookieDays);
				}

				if (fireAfterClose !== "") {
					jQuery(window).trigger(fireAfterClose,[$closeSelector,$this]);
				}
			};

			if (typeof data.cookieName != "undefined") {
				var $cookieDays = false;
				if (typeof data.cookieDays != "undefined") {
					$cookieDays = data.cookieDays;
				}

				if (readCookie(data.cookieName+'_'+$closeType)) {
					close();
				}
			}

			e.preventDefault();
			e.stopPropagation();

			close();
			$this.blur();

			$this.data('closer-attached',true);

		} // data is empty check
	//} // closer atttached check
});