/*!
 * BRUUT™ Framework
 * @version: 2.7
 * @copyright: Onstuimig
 * @author: Onstuimig Interactieve Communicatie - onstuimig.nl
 * @preserve
 */

/* Import (jQuery) plugins */

	// @codekit-append "vendor/modernizr-dev.js";
	// @codekit-append "vendor/velocity.js";
	// @codekit-append "vendor/viewports-units-buggyfill.0.5.4.js";
	// @codekit-append "vendor/ismobile.js";
	// @codekit-append "vendor/flickity.pkgd.min.js";

/* Onstuimig custom plugins - Requires Velocity.js */

    // @codekit-append "../bruut-kits/CoreKit/js/scrollMonitor.js";
    // @codekit-append "../bruut-kits/CoreKit/js/ImagePack.js";

/* Template specific functions */

    // @codekit-append "functions/nativeSelect.js";
    // @codekit-append "functions/isMobileCheck.js";
    // @codekit-append "functions/linksToNewWindow.js";
    // @codekit-append "functions/responsiveImages.js";
    // @codekit-append "functions/antiConsoleErrors.js";
    // @codekit-append "functions/carousel.js";

/* Import Bruut Kit JS here */
