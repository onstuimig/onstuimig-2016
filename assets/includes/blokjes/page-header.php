<div class="page-section page-section--promo page-header">

	<!--
		@Jeroen: formaten: [16/9]
		data-img-src - standaard, 1920 X 1080
		data-img-src-desktop-large - desktop-large, 1650 X 928
		data-img-src-desktop-large - desktop-small, 1200 X 675
		data-img-src-tablet - tablet, 1024 X 576
		data-img-src-small - small, 768 X 432

		+ data-bg-position als optie!
	-->

	<div class="cover-image js" data-img-src="assets/img/beeld/vrouwke.jpg" data-bg-position="left top"></div>

	<section class="grid grid--spacy">
		<header class="page-section__header">
			<h2 class="page-section__title">Naïviteit moet je koesteren</h2>
			<p class="page-section__desc">Dom doen is eigenlijk best slim.</p>
		</header>
	</section>
</div>

<div class="page-section page-section--article">
	<section class="grid grid--spacy">
		<article>
			<p class="article--intro">Afhakken van een Os, en hoe men daar mede handeldt, en hoe men de stukken best gebruikt. Als de Os doormidden gehakt is, dan moet men ieder zyde nog eens midden door laaten hakken. </p>

			<p>Maar zo, dat de voorste zyde, het kortste is, om dat de ribbenstukken dan veel beeter uitkomen.Het <a href="#">bloedige van den hals</a>, met den Afval; legt men byzonder, en is geschikt tot Huspot; en kan, na dat het uitgetrokken is en een dag of zes in 't zout gelegen heeft, gegeeten worden.</p>

			<div class="button-holder">
				<a href="#" class="big-button contrast">Laat maar zien</a>
			</div>
		</article>
	</section>
</div>
