<div class="page-section page-section--icon background-contrast">
	<section class="grid grid--spacy">
		{exp:icon:infinity class="icon-xl"}
	</section>
</div>

<div class="page-section page-section--title background-contrast">
	<section class="grid grid--spacy">
		<span class="page-section__title">Eenvoud is ons geheim</span>
		<span class="page-section__desc">Makkelijk is het nieuwe moeilijk</span>
	</section>
</div>

<div class="page-section page-section--three-columns background-contrast">
	<section class="grid grid--spacy">
		<div class="row">
			<div class="column third full-tablet summary-box">
				<div class="js responsive-image ratio-16x9">
					<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

					<noscript>
						<img src="assets/img/beeld/1.jpg" alt="Plaatje">
					</noscript>
				</div>

				<div class="summary-box__content">
					<h3 class="title">Intelligente kop</h3>
					<p>Dat doen we dus zo lorem ipsum consectetur <a href="#">adipisicing</a> elit. Placeat eius, beatae ipsum recusandae doloribus ad iure, animi vel maiores nisi.
				</div>
			</div>

			<div class="column third full-tablet summary-box">
				<div class="js responsive-image ratio-16x9">
					<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

					<noscript>
						<img src="assets/img/beeld/1.jpg" alt="Plaatje">
					</noscript>
				</div>

				<div class="summary-box__content">
					<h3 class="title">Intelligente kop</h3>
					<p>Dat doen we dus zo lorem ipsum consectetur <a href="#">adipisicing</a> elit. Placeat eius, beatae ipsum recusandae doloribus ad iure, animi vel maiores nisi.
				</div>
			</div>

			<div class="column third full-tablet summary-box">
				<div class="js responsive-image ratio-16x9">
					<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

					<noscript>
						<img src="assets/img/beeld/1.jpg" alt="Plaatje">
					</noscript>
				</div>

				<div class="summary-box__content">
					<h3 class="title">Intelligente kop</h3>
					<p>Dat doen we dus zo lorem ipsum consectetur <a href="#">adipisicing</a> elit. Placeat eius, beatae ipsum recusandae doloribus ad iure, animi vel maiores nisi.
				</div>
			</div>
		</div>
	</section>
</div>

<hr>

<div class="page-section page-section--three-columns background-onstuimig">
	<section class="grid grid--spacy">
		<div class="row">
			<div class="column third full-tablet summary-box">
				<div class="js responsive-image ratio-16x9">
					<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

					<noscript>
						<img src="assets/img/beeld/1.jpg" alt="Plaatje">
					</noscript>
				</div>

				<div class="summary-box__content">
					<h3 class="title">Intelligente kop</h3>
					<p>Dat doen we dus zo lorem ipsum consectetur <a href="#">adipisicing</a> elit. Placeat eius, beatae ipsum recusandae doloribus ad iure, animi vel maiores nisi.
				</div>
			</div>

			<div class="column third full-tablet summary-box">
				<div class="js responsive-image ratio-16x9">
					<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

					<noscript>
						<img src="assets/img/beeld/1.jpg" alt="Plaatje">
					</noscript>
				</div>

				<div class="summary-box__content">
					<h3 class="title">Intelligente kop</h3>
					<p>Dat doen we dus zo lorem ipsum consectetur <a href="#">adipisicing</a> elit. Placeat eius, beatae ipsum recusandae doloribus ad iure, animi vel maiores nisi.
				</div>
			</div>

			<div class="column third full-tablet summary-box">
				<div class="js responsive-image ratio-16x9">
					<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

					<noscript>
						<img src="assets/img/beeld/1.jpg" alt="Plaatje">
					</noscript>
				</div>

				<div class="summary-box__content">
					<h3 class="title">Intelligente kop</h3>
					<p>Dat doen we dus zo lorem ipsum consectetur <a href="#">adipisicing</a> elit. Placeat eius, beatae ipsum recusandae doloribus ad iure, animi vel maiores nisi.
				</div>
			</div>
		</div>
	</section>
</div>

<hr>

<div class="page-section page-section--three-columns background-dark">
	<section class="grid grid--spacy">
		<div class="row">
			<div class="column third full-tablet summary-box">
				<div class="js responsive-image ratio-16x9">
					<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

					<noscript>
						<img src="assets/img/beeld/1.jpg" alt="Plaatje">
					</noscript>
				</div>

				<div class="summary-box__content">
					<h3 class="title">Intelligente kop</h3>
					<p>Dat doen we dus zo lorem ipsum consectetur <a href="#">adipisicing</a> elit. Placeat eius, beatae ipsum recusandae doloribus ad iure, animi vel maiores nisi.
				</div>
			</div>

			<div class="column third full-tablet summary-box">
				<div class="js responsive-image ratio-16x9">
					<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

					<noscript>
						<img src="assets/img/beeld/1.jpg" alt="Plaatje">
					</noscript>
				</div>

				<div class="summary-box__content">
					<h3 class="title">Intelligente kop</h3>
					<p>Dat doen we dus zo lorem ipsum consectetur <a href="#">adipisicing</a> elit. Placeat eius, beatae ipsum recusandae doloribus ad iure, animi vel maiores nisi.
				</div>
			</div>

			<div class="column third full-tablet summary-box">
				<div class="js responsive-image ratio-16x9">
					<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

					<noscript>
						<img src="assets/img/beeld/1.jpg" alt="Plaatje">
					</noscript>
				</div>

				<div class="summary-box__content">
					<h3 class="title">Intelligente kop</h3>
					<p>Dat doen we dus zo lorem ipsum consectetur <a href="#">adipisicing</a> elit. Placeat eius, beatae ipsum recusandae doloribus ad iure, animi vel maiores nisi.
				</div>
			</div>
		</div>
	</section>
</div>