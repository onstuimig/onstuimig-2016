<div class="page-section page-section--icon background-contrast">
	<section class="grid grid--spacy">
		{exp:icon:infinity class="icon-xl"}
	</section>
</div>

<div class="page-section page-section--title background-contrast">
	<section class="grid grid--spacy">
		<span class="page-section__title">het eindpunt is ons begin</span>
		<span class="page-section__desc">we ontwerpen direct in de browser <br/> Zodat je vooral ook voelt hoe alles werkt</span>
	</section>
</div>

<div class="page-section page-section--image background-contrast">
	<section class="grid grid--spacy">
		<div class="js responsive-image ratio-16x9">

			<!--
				@Jeroen: formaten: [16/9]
				data-img-src - standaard, 1367 X 769
				data-img-src-tablet - tablet, 1024 X 576
				data-img-src-mobile - mobile, 480 X 270
			-->

			<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

			<noscript>
				<img src="assets/img/beeld/1.jpg" alt="Plaatje">
			</noscript>
		</div>
	</section>
</div>

<div class="page-section page-section--text background-contrast">
	<section class="grid grid--spacy">
		<article class="intro">
			<p>Afhakken van een Os, en hoe men daar mede handeldt, en <a href="#">hoe men de stukken best gebruikt</a>. Als de Os doormidden gehakt is, dan moet men ieder zyde nog eens midden door laaten hakken. </p>
		</article>
	</section>
</div>

<div class="page-section page-section--text background-contrast">
	<section class="grid grid--spacy">
		<article>
			<p>Afhakken van een Os, en hoe men daar mede handeldt, en <a href="#">hoe men de stukken best gebruikt</a>. Als de Os doormidden gehakt is, dan moet men ieder zyde nog eens midden door laaten hakken. </p>
		</article>
	</section>
</div>

<div class="page-section page-section--button background-contrast">
	<section class="grid grid--spacy">
		<a href="#" class="big-button">Laat maar zien</a>
	</section>
</div>