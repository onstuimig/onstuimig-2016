<?php
	function icon($name, $class = null, $return = false) {
		$svg = '<svg class="svg-icon '.$class.'">
			<use xlink:href="#icon--'.$name.'"></use>
		</svg>';

		if($return)
			return $svg;

		echo $svg;
	}
?>

<?php
ob_start();
?>

<!DOCTYPE html>
<html class="no-js">
	<head>
		<title>Onstuimige landing</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="format-detection" content="telephone=no">
		<link href="assets/min/all.min.css?version=<?php echo date('Y-m-d');?>" rel="stylesheet" media="screen">
		<meta charset="utf-8">

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>

	<body>
		<?php include('assets/img/icons/symbol-defs.svg') ?>

	<a href="#main-content" class="visually-hidden">Naar content</a>

	<main class="site-content">

		<header class="global-navigation">
			<a href="#">
				<img src="assets/img/icons/logo-contrast.svg" alt="Logo Onstuimig" class="global-navigation__logo responsive">
			</a>
		</header>

	<!-- slide-out-panel-push-wrapper, for the slide out menu on < 768 -->
	<div class="slideout-panel" id="panel">
		<span class="slideout-panel-close-trigger" aria-hidden="true"></span>
