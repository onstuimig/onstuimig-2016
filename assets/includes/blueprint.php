<?php

/*
name : 			BRUUT BLUEPRINT
version : 		0.1
created by : 	Tim
Notes:			Dummy html content by http://html-ipsum.com/
*/


$page_dummycontent = '
<h2>Header Level 2</h2>

<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<h3>Header Level 3</h3>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>

<pre><code>
#header h1 a {
	display: block;
	width: 300px;
	height: 80px;
}
</code></pre>
';

$article_dummycontent = '
<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>

';

$blueprint = array(

	/* channel PAGE */
	"page" => array(
		"entry_id" 		=> 20,
		"title" 		=> "test1",
		"visual" 		=> "http://lorempixel.com/1900/320",
		"intro" 		=> "lorem",
		"content" 		=> addslashes($page_dummycontent),
		"categories" 	=> array("aap","noot","mies")
	),

	/* channel ARTICLE */
	"articles" => array(
        "jopie" => array(
    		"entry_id" 		=> 1,
    		"title" 		=> "Article title 1",
    		"visual_url" 	=> "http://lorempixel.com/1900/320",
    		"intro" 		=> "lorem",
    		"content" 		=> addslashes($article_dummycontent),
    		"categories" 	=> array("aap","noot","mies")
        ),

        "homepage" => array(
    		"entry_id" 		=> 2,
    		"title" 		=> "Article title 2",
    		"visual_url" 	=> "http://lorempixel.com/1900/320",
    		"intro" 		=> "lorem",
    		"content" 		=> addslashes($article_dummycontent),
    		"categories" 	=> array("aap","noot","mies")
        ),

	),

	/* 'low' loose VARIABLES */
	"var" => array(
        "project_name"      => "Bruut Framework",
		"analytics_code"    => "ua-1234",
		"contact_email"     => "info@onstuimig.nl"
	)

);

echo json_encode($blueprint);

?>
