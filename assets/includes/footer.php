</div><!-- /.site-wrap -->

</main>

<footer></footer>

	<script src="assets/min/all.min.js"></script>

</body>
</html>

<?php
if (!isset($_GET['expvars'])) {
	$page = ob_get_contents();

	$page = preg_replace("/{site_url}|{exp:trans}|{\/exp:trans}/",'',$page);
	$page = preg_replace_callback("/{exp:icon:(.*?)(?:\s(?:.*?)class=(?:\"|\')(.*?)(?:\"|\')(?:.*?))?}/", "replace_icon", $page);
	ob_end_clean();
	echo $page;
} else {
	if (!isset($_GET['allvars'])) {
		$page = ob_get_contents();
		$page = preg_replace("/{site_url}/",'',$page);
		ob_end_clean();
		echo $page;
	}
}

function replace_icon($matches){
	if(function_exists('icon'))
		return icon($matches[1], $matches[2], true);

	return $matches[0];
}
?>
