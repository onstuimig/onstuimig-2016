<div class="page-section page-section--text background-contrast">
	<section class="grid grid--spacy">
		<article>
			<p>Afhakken van een Os, en hoe men daar mede handeldt, en <a href="#">hoe men de stukken best gebruikt</a>. Als de Os doormidden gehakt is, dan moet men ieder zyde nog eens midden door laaten hakken. </p>
		</article>
	</section>
</div>

<hr>

<div class="page-section page-section--text background-onstuimig">
	<section class="grid grid--spacy">
		<article>
			<p>Afhakken van een Os, en hoe men daar mede handeldt, en <a href="#">hoe men de stukken best gebruikt</a>. Als de Os doormidden gehakt is, dan moet men ieder zyde nog eens midden door laaten hakken. </p>
		</article>
	</section>
</div>

<hr>

<div class="page-section page-section--text background-dark">
	<section class="grid grid--spacy">
		<article>
			<p>Afhakken van een Os, en hoe men daar mede handeldt, en <a href="#">hoe men de stukken best gebruikt</a>. Als de Os doormidden gehakt is, dan moet men ieder zyde nog eens midden door laaten hakken. </p>
		</article>
	</section>
</div>
