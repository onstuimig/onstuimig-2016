<div class="page-section page-section--image background-contrast">
	<section class="grid grid--spacy">
		<div class="js responsive-image" style="max-width: 1600px;">
			<div class="responsive-image-holder" style="padding-bottom: 56.25%;">
				<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

				<noscript>
					<img src="assets/img/beeld/1.jpg" alt="Plaatje">
				</noscript>
			</div>

			<!--
				@Jeroen: formaten: [16/9]
				data-img-src - standaard, 1367 X 769
				data-img-src-tablet - tablet, 1024 X 576
				data-img-src-mobile - mobile, 480 X 270
			-->
		</div>
	</section>
</div>

<hr>

<div class="page-section page-section--image background-onstuimig">
	<section class="grid grid--spacy">
		<div class="js responsive-image" style="max-width: 1600px;">
			<div class="responsive-image-holder" style="padding-bottom: 56.25%;">
				<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

				<noscript>
					<img src="assets/img/beeld/1.jpg" alt="Plaatje">
				</noscript>
			</div>

			<!--
				@Jeroen: formaten: [16/9]
				data-img-src - standaard, 1367 X 769
				data-img-src-tablet - tablet, 1024 X 576
				data-img-src-mobile - mobile, 480 X 270
			-->
		</div>
	</section>
</div>

<hr>

<div class="page-section page-section--image background-dark">
	<section class="grid grid--spacy">
		<div class="js responsive-image" style="max-width: 1600px;">
			<div class="responsive-image-holder" style="padding-bottom: 56.25%;">
				<img data-img-src="assets/img/beeld/1.jpg" alt="Plaatje">

				<noscript>
					<img src="assets/img/beeld/1.jpg" alt="Plaatje">
				</noscript>
			</div>

			<!--
				@Jeroen: formaten: [16/9]
				data-img-src - standaard, 1367 X 769
				data-img-src-tablet - tablet, 1024 X 576
				data-img-src-mobile - mobile, 480 X 270
			-->
		</div>
	</section>
</div>
