<div class="page-section page-section--promo background-onstuimig">

	<!--
		@Jeroen: formaten: [16/9]
		data-img-src - standaard, 1920 X 1080
		data-img-src-desktop-large - desktop-large, 1650 X 928
		data-img-src-desktop-small - desktop-small, 1200 X 675
		data-img-src-tablet - tablet, 1024 X 576
		data-img-src-small - small, 768 X 432

		+ data-bg-position als optie!
	-->

	<div class="cover-image js" data-img-src="assets/img/beeld/vrouwke.jpg" data-bg-position="left top"></div>

	<section class="grid grid--spacy">
		<header class="page-section__header">

			<!-- Le icon -->
			{exp:icon:web-love class="icon-xl"}

			<h2 class="page-section__title">Zij blij, jij ook blij</h2>
			<p class="page-section__desc">Blije bezoekers doen wat jij hoopt <br />  <a href="#">hakken</a> Dus geven wij ze een goed gevoel</p>
		</header>

		<article class="article--intro">
			<p>
				Afhakken van een Os, en hoe men daar mede handeldt, en hoe men de stukken best gebruikt. Als de Os doormidden gehakt is, dan moet men ieder zyde nog eens midden door laaten <a href="#">hakken</a>.
			</p>
		</article>
	</section>
</div>
