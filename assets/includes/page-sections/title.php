<div class="page-section page-section--title background-contrast">
	<section class="grid grid--spacy">
		<span class="page-section__title">het eindpunt <a href="#">is ons</a> begin</span>
		<span class="page-section__desc">we ontwerpen direct in de browser <br> Zodat je <a href="#">vooral</a> ook voelt hoe alles werkt</span>
	</section>
</div>

<hr>

<div class="page-section page-section--title background-onstuimig">
	<section class="grid grid--spacy">
		<span class="page-section__title">het eindpunt <a href="#">is ons</a> begin</span>
		<span class="page-section__desc">we ontwerpen direct in de browser <br> Zodat je <a href="#">vooral</a> ook voelt hoe alles werkt</span>
	</section>
</div>

<hr>

<div class="page-section page-section--title background-dark">
	<section class="grid grid--spacy">
		<span class="page-section__title">het eindpunt <a href="#">is ons</a> begin</span>
		<span class="page-section__desc">we ontwerpen direct in de browser <br> Zodat je <a href="#">vooral</a> ook voelt hoe alles werkt</span>
	</section>
</div>
