<div class="page-section page-section--footer background-onstuimig">
	<section class="grid grid--spacy">
		<ul class="footer-links">
			<li><a href="#">Instagram</a></li>
			<li><a href="#">Lorem ipsum</a></li>
			<li><a href="#">consectetur adip</a></li>
			<li><a href="#">Expedita molesti</a></li>
			<li><a href="#">minus dolorem</a></li>
			<li><a href="#">voluptatum labore</a></li>
			<li><a href="#">possimus nulla</a></li>
			<li><a href="#">similique cumque</a></li>
			<li><a href="#">porro doloribus</a></li>
		</ul>

		<a href="#">
			<img src="assets/img/icons/logo-contrast.svg" alt="Logo Onstuimig" class="footer-links__logo responsive">
		</a>
	</section>
</div>

<hr>

<div class="page-section page-section--footer background-dark">
	<section class="grid grid--spacy">
		<ul class="footer-links">
			<li><a href="#">Instagram</a></li>
			<li><a href="#">Lorem ipsum</a></li>
			<li><a href="#">consectetur adip</a></li>
			<li><a href="#">Expedita molesti</a></li>
			<li><a href="#">minus dolorem</a></li>
			<li><a href="#">voluptatum labore</a></li>
			<li><a href="#">possimus nulla</a></li>
			<li><a href="#">similique cumque</a></li>
			<li><a href="#">porro doloribus</a></li>
		</ul>

		<a href="#">
			<img src="assets/img/icons/logo-contrast.svg" alt="Logo Onstuimig" class="footer-links__logo responsive">
		</a>
	</section>
</div>
