<section class="page-section page-section--carousel js background-dark"> <!-- fixen jongen -->

	<header>
		<h1 class="page-section__title">de beste online ervaring</h1>
		<p class="page-section__desc">We maken jouw digitale dienst succesvol</p>
	</header>

	<div class="projects-switcher no-animate js-projects-switcher">
		<div class="project">
			<div class="inner">

				<div class="responsive-image scroller js">
					<img data-img-src="assets/img/beeld/laplace-site.jpg" alt="Scrolling image">
				</div>

				<ul class="project__meta">
					<li class="title">CLiniclowns</li>
					<li class="description">Mensen komen nu vaker en langer kijken</li>
				</ul>

			</div>
		</div>

		<div class="project">
			<div class="inner">

				<div class="responsive-image scroller js">
					<img data-img-src="assets/img/beeld/site-scroll.png" alt="Scrolling image">
				</div>

				<ul class="project__meta">
					<li class="title">NPO.nl <span class="job">UX Design</span></li>
					<li class="description">Mensen komen nu vaker en langer kijken</li>
				</ul>

			</div>
		</div>

		<div class="project">
			<div class="inner">

				<div class="responsive-image scroller js">
					<img data-img-src="assets/img/beeld/laplace-site.jpg" alt="Scrolling image">
				</div>

				<ul class="project__meta">
					<li class="title">La Place</li>
					<li class="description">Mensen komen nu vaker en langer kijken</li>
				</ul>

			</div>
		</div>

		<div class="project">
			<div class="inner">

				<div class="responsive-image scroller js">
					<img data-img-src="assets/img/beeld/site-scroll.png" alt="Scrolling image">
				</div>

				<ul class="project__meta">
					<li class="title">La Place</li>
					<li class="description">Mensen komen nu vaker en langer kijken</li>
				</ul>
			</div>
		</div>

		<div class="project">
			<div class="inner">

				<div class="responsive-image scroller js">
					<img data-img-src="assets/img/beeld/laplace-site.jpg" alt="Scrolling image">
				</div>

				<ul class="project__meta">
					<li class="title">La Place</li>
					<li class="description">Mensen komen nu vaker en langer kijken</li>
				</ul>
			</div>
		</div>

		<div class="project">
			<div class="inner">

				<div class="responsive-image scroller js">
					<img data-img-src="assets/img/beeld/site-scroll.png" alt="Scrolling image">
				</div>

				<ul class="project__meta">
					<li class="title">La Place</li>
					<li class="description">Mensen komen nu vaker en langer kijken</li>
				</ul>
			</div>
		</div>

		<div class="project">
			<div class="inner">

				<div class="responsive-image scroller js">
					<img data-img-src="assets/img/beeld/laplace-site.jpg" alt="Scrolling image">
				</div>

				<ul class="project__meta">
					<li class="title">La Place</li>
					<li class="description">Mensen komen nu vaker en langer kijken</li>
				</ul>
			</div>
		</div>
	</div>
</section>