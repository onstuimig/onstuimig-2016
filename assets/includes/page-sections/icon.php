<div class="page-section page-section--icon background-contrast">
	<section class="grid grid--spacy">
		{exp:icon:infinity class="icon-xl"}
	</section>
</div>

<hr>

<div class="page-section page-section--icon background-onstuimig">
	<section class="grid grid--spacy">
		{exp:icon:infinity class="icon-xl"}
	</section>
</div>

<hr>

<div class="page-section page-section--icon background-dark">
	<section class="grid grid--spacy">
		{exp:icon:infinity class="icon-xl"}
	</section>
</div>
