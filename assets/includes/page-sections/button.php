<div class="page-section page-section--button background-contrast">
	<section class="grid grid--spacy">
		<a href="#" class="big-button">Laat maar zien</a>
	</section>
</div>

<hr>

<div class="page-section page-section--button background-onstuimig">
	<section class="grid grid--spacy">
		<a href="#" class="big-button">Laat maar zien</a>
	</section>
</div>

<hr>

<div class="page-section page-section--button background-dark">
	<section class="grid grid--spacy">
		<a href="#" class="big-button">Laat maar zien</a>
	</section>
</div>
