# Bruut Framework

Developed as a solid foundation for our prototypes, Bruut™ is build to be fast, flexible and solid.

The framework comes with some standard components like grid, form elements and helper classes. After each prototype that we build, we update the framework as well. Bruut™ is a framework with a small codebase (CSS: 7.16kb via Gzip, JS: 33kb via Gzip that is rock solid and heavily tested for everyday use.

Bruut™ is build on Twig, Sass, Typescript and jQuery.



### Configuring your dev environment

Configuring a dev environment for this project is easy:

- Download Prepros or Codekit (paid) and drag and drop the folder of this project in the app.
- Make sure that the CSS is compiled via `all.scss` to the production folder `assets/min/all.min.css`.
- Compile the JS via `all.js` to the production folder `assets/min/all.min.js`.
- You're good to go :)

> Tip: Use LibSass when possible (it’s a lot faster than the standard Sass compiler. Bruut™ can handle LibSass out of the box).



## Bruut ImageKit

[Bitbucket repository ->](https://bitbucket.org/onstuimig/bruut-imagekit.git)

The ImageKit brings responsive & lazy (background) images to Bruut.

Installation is simple: Clone the GIT-repo from above and place it in your project folder under `assets/bruut-kits/ImageKit/` or use as a submodule.

In `assets/sass/all.scss`, uncomment following line to the 'Import Bruut Kits' section (below the import of CoreKit):

``` scss
//
// Import Bruut Kits
// --------------------------------------------------

	@import "../bruut-kits/CoreKit/sass/CoreKit";
	// @import "../bruut-kits/ImageKit/sass/ImageKit"; <-- Uncomment this line
```

In `assets/js/all.js`, add the following code-snippet (below the comment 'Import Bruut Kit JS here') with the double slashes. This will work for both Codekit and Prepros.

``` javascript
// @codekit-append "../bruut-kits/ImageKit/js/scrollMonitor.js";
// @codekit-append "../bruut-kits/ImageKit/js/ImageKit.js";
```



#### Using ImageKit

##### Javascript

You can call the class  ` LazyResponsiveImage()`  like below. Note: You don't need to pass a configuration object (there's a default configuration object available in the class), but if you want to load background-images, you'll need to pass the `type` 'background' key.

``` javascript
var images          = [];

// We call the responsive-image container
jQuery('.js-responsive-image').each(function(index, element) {
	images[index] = new LazyResponsiveImage(element, {
     	
      	// String, 'image' or 'background'
		type: 				'image',
      	
      	// At which point in pixels below the viewport are the images needed?
      	// use -125 for loading the images when they are 125px in the viewport
      	viewportOffset: 	250, 			
      	
      	// Class that will be added to the container when the image is loaded
      	loadedClass: 		'is-loaded',
      
      	// If the container has this class, the image will be loaded on page load
		noLazyClass: 		'no-lazy',	
      
      	// The data-attribute for the standard image (largest size). 
      	// The values from the imageSizes key will be added to this prefix, 
      	// so data-img-src-mobile for example
		baseDataAttribute:	'data-img-src',
      	
      	// Responsive image sizes. Use a screen width (number) as key 
      	// and your data-img-src-[value] as value (string).
		imageSizes: {
			481 		:	'mobile',
			768 		:	'small',
			1024		:	'tablet'
		}									
	});
});
```



##### HTML

##### Global options

- You can choose any number of image variations on screen sizes, just add them in the configuration object when you call the `LazyResponsiveImage` function. Start with the smallest screen size first.
- If you don't want to lazy load the (background) image, add the class `'no-lazy'` to the container. `lazyResponsiveImage` will notice the class and set the source directly on page load.
- As soon as the image is loaded, the class `is-loaded` will be added to the container. The inline images will then be faded in via CSS transitions.



The structure for a responsive image looks like this:

``` html
<div class="responsive-image ratio-16x9 js-responsive-image">
	<img data-img-src="http://lorempixel.com/1600/900/cats" data-img-src-mobile="http://lorempixel.com/160/90/cats">
</div>
```

##### Options

- The ratio must be a css class (you'll have to make it yourself if it isn't 16x9). You can define it with the `mixin @include aspect-ratio(x,x)`. The aspect ratio defines the width and height of the image container. The image is set absolute in the image container, to prevent layout reflows on image load.
  
  ​

The structure for a responsive background-image looks like this:

``` html
<div class="cover-image js-cover-image" data-img-src="standard" data-img-src-small="medium" data-img-src-mobile="mini" data-bg-position="center center">
  	<span class="cover-image--mask" data-mask-strength="5"></span>
	<!-- Your content -->
</div>
```

##### Background options

-  To position the background-image, use the `data-bg-position` attribute. Make combinations of left, center, right, bottom and top.
- If you want to 'mask' the background image, use the optional `cover-image—mask` and give it a `data-mask-strength` of 1 - 5. This will add a black transparent layer (transparancy of 10% - 50%) over the background-image. Ofcourse, you can add your own gradient or layer as well.

