<?php include('assets/includes/header.php'); ?>

	<!-- If you want to use icons, do it like so: -->
	<!-- {exp:icon:icon-naam} en {exp:icon:icon-naam class="class-naam"} -->

	<?php include('assets/includes/page-sections/page-footer.php') ?>

	<?php include('assets/includes/page-sections/carousel.php') ?>

	<?php include('assets/includes/page-sections/promo.php') ?>

	<?php include('assets/includes/page-sections/page-header.php') ?>

	<?php include('assets/includes/page-sections/drie-kolommen.php') ?>

	<?php include('assets/includes/page-sections/text.php') ?>

	<?php include('assets/includes/page-sections/text-intro.php') ?>

	<?php include('assets/includes/page-sections/image.php') ?>

	<?php include('assets/includes/page-sections/title.php') ?>

	<?php include('assets/includes/page-sections/icon.php') ?>

	<?php include('assets/includes/page-sections/button.php') ?>

<?php include('assets/includes/footer.php'); ?>
