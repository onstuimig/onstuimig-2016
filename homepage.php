<?php include('assets/includes/header.php'); ?>

	<!-- If you want to use icons, do it like so: -->
	<!-- {exp:icon:icon-naam} en {exp:icon:icon-naam class="class-naam"} -->

	<?php include('assets/includes/page-sections/page-header.php') ?>

	<?php include('assets/includes/page-sections/promo.php') ?>

	<?php include('assets/includes/blokjes/artikel.php') ?>

	<?php include('assets/includes/blokjes/3-kolommen.php') ?>

	<?php include('assets/includes/blokjes/kleur-onstuimig.php') ?>

	<?php include('assets/includes/blokjes/beeld.php') ?>

	<?php include('assets/includes/blokjes/kleur-zwart.php') ?>

<?php include('assets/includes/footer.php'); ?>
