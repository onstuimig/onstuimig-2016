<?php include('assets/includes/header.php'); ?>

<style>
.form-group.has-errors .form-element input,
.form-element.has-errors input {
	border: 1px solid red;
}

section.grid {
	max-width: 600px;
}

.form-group.is-ok .form-element input,
.form-element.is-ok input {
	background: #8bc34a;
	color: #fff !important;
	border-color: #8bc34a !important;
}

.form-element .message {
	display: none;
	padding: 12px 16px;
	color: #fff;
	font-size: 18px;
	border-radius: 6px;
}

.form-element.is-ok .message,
.message.is-ok {
	background: #8bc34a;
	display: inline-block;
}

.form-element.has-errors .message,
.message.has-errors {
	background: #e91e63;
	display: inline-block;
}

.hidden { display:none; }
</style>

	<form class="form" validate-form="myForm" novalidate autocomplete="off">

		<section class="grid my-4">
			<div class="row">
				<div class="column">
					<button type="submit" class="button p-2 background-success color-contrast">Valideren</button>
				</div>
			</div>
		</section>

		<hr>

		<section class="grid my-4">
			<div class="row">
				<div class="column">
					<h2>Custom validatie</h2>
					<p>Validatie: <code>hex</code></p>
				</div>

				<div class="column">
					<div class="form-element">
						<input type="email" class="input" v-name="testy" v-props="hex">
						<div class="message mt-4" v-message-for="testy"></div>
					</div>
				</div>
			</div>
		</section>

		<hr>

		<section class="grid my-4">
			<div class="row">
				<div class="column">
					<h2>Emailvalidatie</h2>
					<p>Validatie: <code>email, min-length:2</code></p>
				</div>

				<div class="column">
					<div class="form-element">
						<input type="email" class="input" v-name="email" v-props="email, min-length:2">
						<div class="message mt-4" v-message-for="email"></div>
					</div>
				</div>
			</div>
		</section>

		<hr>

		<section class="grid my-4">
			<div class="row">
				<div class="column">
					<h2>Nummervalidatie</h2>
					<p>Validatie: <code>characters:numeric, min-length:2</code></p>
				</div>

				<div class="column">
					<div class="form-element">
						<input type="text" class="input" v-name="numeric" v-props="characters:numeric, min-length:2">

						<div class="message mt-4" v-message-for="numeric"></div>

					</div>
				</div>
			</div>
		</section>

		<hr>

		<section class="grid my-4">
			<div class="row">
				<div class="column">
					<h2>Lettervalidatie</h2>
					<p>Validatie: <code>characters:alpha, min-length:2</code></p>
				</div>

				<div class="column">
					<div class="form-element">
						<input type="text" class="input" v-name="alpha" v-props="characters:alpha, min-length:2">

						<div class="message mt-4" v-message-for="alpha"></div>

					</div>
				</div>
			</div>
		</section>

		<hr>

		<section class="grid my-4">
			<div class="row">
				<div class="column">
					<h2>Combine input</h2>
					<p>Validatie: <code>date, min-length:2</code></p>
				</div>

				<div class="column half">
					<div class="form-group">
						<div class="form-element">
							<div class="row">
								<div class="column third">
									<div class="form-element">
										DAG
										<input type="text" class="input" v-name="geboorteDatum" v-combine="geboorteDatum" maxlength="2" >
									</div>
								</div>

								<div class="column third">
									<div class="form-element">
										MAAND
										<input type="text" class="input" v-name="geboorteDatum" v-combine="geboorteDatum" maxlength="2" >
									</div>
								</div>

								<div class="column third">
									<div class="form-element">
										JAAR
										<input type="text" class="input" v-name="geboorteDatum" v-combine="geboorteDatum" maxlength="4" >
									</div>
								</div>

								<div class="message" v-message-for="geboorteDatum"></div>
							</div>
						</div>
					</div>


					<div class="row">
						<div class="column">
							<p><code>Glued input</code></p>
							<div class="form-element">
								<input type="text"
									class="input"
									v-glue-character="-"
									v-glue-result="geboorteDatum"
									name="geboorteDatum"
									v-props="date">
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="grid my-4">
			<div class="row">
				<div class="column">
					<h2>Combine input</h2>
					<p>Validatie: <code>postcode, min-length:2</code></p>
				</div>

				<div class="column half form-group">
					<div class="form-element">
						<div class="row">
							<div class="column two-third">
								<div class="form-element">
									<input type="text" class="input" v-name="aapje2" v-props="characters:numeric, min-length:4, max-length:4" v-combine="aapje" maxlength="4" >
								</div>
							</div>

							<div class="column third">
								<div class="form-element">
									<input type="text" class="input" v-combine="aapje" maxlength="2" >
								</div>
							</div>

							<div class="message" v-message-for="aapje"></div>
						</div>
					</div>

					<div class="row">
						<div class="column">
							<p><code>Glued input</code></p>
							<div class="form-element">
								<input type="text" class="input" v-glue-character=" " v-glue-result="aapje" v-props="postcode">
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="grid my-4">
			<div class="row">
				<div class="column">
					<h2>Checkboxes</h2>
					<p>Validatie: <code>min-checked:2, max-checked: 3</code></p>
				</div>

				<div class="column">
					<div class="form-element" v-group="group:checkMinMaxLength, min-checked:2, max-checked: 3">

						<input type="checkbox" name="checkje[]" v-group-child="checkMinMaxLength">
						<input type="checkbox" name="checkje[]" v-group-child="checkMinMaxLength">
						<input type="checkbox" name="checkje[]" v-group-child="checkMinMaxLength">
						<input type="checkbox" name="checkje[]" v-group-child="checkMinMaxLength">


					</div>

					<div class="form-element">
						<div class="message" v-message-for="checkMinMaxLength"></div>
					</div>
				</div>
			</div>
		</section>

		<ul class="errors hidden" v-errors>
			<li v-error-for="email">Geen geldig e-mailadres</li>
			<li v-ok-for="email">
				<div class="unit">
					Helemaal ok!
				</div>
			</li>

			<li v-error-for="testy">Hex waarde nodig!</li>
			<li v-ok-for="testy">Helemaal ok!</li>

			<li v-error-for="alpha">Minimaal 2 letters</li>
			<li v-ok-for="alpha">Helemaal ok!</li>

			<li v-error-for="numeric">Minimaal 2 cijfers</li>
			<li v-ok-for="numeric">Helemaal ok!</li>

			<li v-error-for="aapje">4 cijfers, twee letters</li>
			<li v-ok-for="aapje">Helemaal ok!</li>

			<li v-error-for="checkMinMaxLength">Minimaal 2, maximaal 3 items</li>
			<li v-ok-for="checkMinMaxLength">Helemaal ok!</li>

			<li v-error-for="geboorteDatum">Geboortedatum nodig</li>
			<li v-ok-for="geboorteDatum">Helemaal ok!</li>
		</ul>
	</form>

<?php include('assets/includes/footer.php'); ?>
