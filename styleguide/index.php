<?php

	include 'parsedown.php';

	$Parsedown = new Parsedown();

	$sections = array();

	$i = 0;
	foreach (glob("source/*", GLOB_ONLYDIR) as $filename) {
		$section_name = explode(' - ',basename($filename))[1];
		$sections[$i]['main_section'] = $section_name;

		$j = 0;
		foreach (glob($filename . "/*.md") as $markdown) {
			$section_name = explode(' - ',basename($markdown, '.md'))[1];
			$sections[$i]['sub_section'][$j] = $section_name;

			$sections[$i]['content'][$j] = $Parsedown->text(file_get_contents($markdown));

			++$j;
		}
		++$i;
	}
?>

	<html>
	<head>
		<title>Bruutst.yleguide</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link media="all" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.0/normalize.min.css" type="text/css" />
		<link media="all" rel="stylesheet" href="http://client.onstuimig.nl/__projecttemplate.css" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>


		<style>

		.content[id*="section-"]:target {
			display: block;
		}

		.menu {
			width: 200px;
			position: fixed;
			left:20px;
			top:20px;
		}

		.menu li {
			display: block;
		}

		</style>
	</head>

	<body>

		<div class="menu">
		<?php
		echo '<ul>';
		foreach($sections as $section_number => $section) {

			echo '<li>';
			echo '<a href="#section-' .  $section_number. '">';
			echo $sections[$section_number]['main_section'];
			echo '</a>';

			$i = 0;
			echo '<ul>';
			foreach($sections[$section_number]['sub_section'] as $sub_number => $sub_section) {

				echo '<li>';
				echo '<a href="#section-' . $section_number . '-' . $sub_number . '">';
				echo $sub_section;
				echo '</a>';
				echo '</li>';

				++$i;
			}
			echo '</ul>';
			echo '</li>';

		}
		echo '</ul>';
		?>
		</div>

		<div class="wrap readme">
		<?php

		foreach($sections as $section_number => $section) {

			echo '<h1 id="section-' . $section_number . '">';
			echo $sections[$section_number]['main_section'];
			echo '</h1>';

			$i = 0;
			foreach($sections[$section_number]['sub_section'] as $sub_number => $sub_section) {

				echo '<div id="section-' . $section_number . '-' . $sub_number . '" class="content ' . ($i > 0 ? 'hide' : false) . '">';

				echo '<h2>';
				echo $sub_section;
				echo '</h2>';

				echo $sections[$section_number]['content'][$sub_number];

				echo '</div>';
				++$i;
			}
		}
		?>
		</div>
	</body>
	</html>